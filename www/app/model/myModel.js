Ext.define('app.model.myModel', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'guestId', type: 'string' },
            { name: 'guestName', type: 'string' },
            { name: 'guestOrigin', type: 'string' }
            ]
    }
});


