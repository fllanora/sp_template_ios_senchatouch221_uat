Ext.define('app.view.myList', {
    extend: 'Ext.List',

    xtype: 'myList',
    
    requires: [
        'app.store.myStore'
    ],

    config: {
        onItemDisclosure: true,
        emptyText: 'No data found!',
        store: 'myStore',

        itemTpl : [
            '<p>{guestName}</p>',
            '<div style="font-size: 0.75em; color: darkgray">{guestOrigin}</div>'
        ].join('')

	}

});
