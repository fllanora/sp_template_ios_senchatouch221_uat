var PARAMS_OFFLINE_USERID = "";

Ext.define('app.view.WebSSOLoginView', {
    extend: 'Ext.form.FormPanel',
    xtype: 'WebSSOLoginView',
    config: {
		   title: 'About',
		   scrollable: false, 
      	   layout: {
                type: 'vbox'
            },
            items: [{
            xtype: 'titlebar',
            title: 'SP Login',
            docked: 'top',
			 ui: 'dark',
			 layout: 'hbox'
	    },   {
                flex: 1
            }, {
                xtype: 'panel',
                height: 400,
                maxWidth: 400,
                minWidth: 300,
                
                items: [{
                    xtype: 'fieldset',
                    title: 'Login with your Spice ID',
                    itemId: 'loginView',
                    defaults: {
                        required: true,
                        labelAlign: 'left',
                        labelWidth: '40%'
                    },
                    items: [{
                        xtype: 'textfield',
                        name: 'userid',
                        id: 'userid',
                        label: 'User ID',
                        value: PARAMS_OFFLINE_USERID,
                        autoCapitalize: false,
                        useClearIcon: true
                    }, {
                        xtype: 'passwordfield',
                        name: 'password',
                        id: 'password',
                        label: 'Password',
                        useClearIcon: true
                    }]
                }, {
                    xtype: 'button',
                    text: 'Login',
                    ui: 'confirm',
                    height: 35,
                    handler: function(){
                        var userID = Ext.getCmp('userid').getValue().toLowerCase();
                        var password = Ext.getCmp('password').getValue();
                        
                        /*****************************************************
                         * ISSUE DESCRIPTION/FIX:
                         * Disable internet connection and enter User id and
                         * keep the cursor in user id field then press login
                         * button.It showing the network required alert,
                         * but the alert is overlapping with user id.
                         * This is happening only in Tablets.
                         *
                         * Make sure to blur the user id field after pressing the
                         * Login button to remove the focus that's causing the problem
                         * in Tablets.
                         *
                         * INCIDENT NUMBER:
                         * IN-SP-2013-0003
                         *
                         * UPDATED BY:
                         * Manimaran Choznan
                         *
                         * Date:
                         * June 11, 2013
                         *
                         *****************************************************/
                        try {
                            Ext.getCmp('password').fieldEl.dom.blur();
                            Ext.getCmp('userid').fieldEl.dom.blur();
                        } 
                        catch (err) {
                        }
                        
                        try {
                            plugins.SoftKeyBoard.hide(function(){
                            }, function(){
                            });
                        } 
                        catch (err) {
                        }
                        
                        var tempUserID = userID;
                        tempUserID = tempUserID.replace(/(^\s*)|(\s*$)/gi, "");
                        tempUserID = tempUserID.replace(/[ ]{2,}/gi, " ");
                        tempUserID = tempUserID.replace(/\n /, "\n");
                        userID = tempUserID;
                        
                        var networkState = navigator.network.connection.type;
                        var states = {};
                        states[Connection.UNKNOWN] = 'Unknown connection';
                        states[Connection.ETHERNET] = 'Ethernet connection';
                        states[Connection.WIFI] = 'WiFi connection';
                        states[Connection.CELL_2G] = 'Cell 2G connection';
                        states[Connection.CELL_3G] = 'Cell 3G connection';
                        states[Connection.CELL_4G] = 'Cell 4G connection';
                        states[Connection.NONE] = 'No network connection';
                        
                        var checkConnection = states[networkState];
                        
                        if ((userID == "") &&
                        (password == "")) {
                            if ((checkConnection == 'Unknown connection') ||
                            (checkConnection == 'No network connection')) {
                                if (PARAMS_STUDENT_ID == "") {
                                    Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                                        navigator.app.exitApp();
                                    }).doComponentLayout();
                                }
                                else {
                                    Ext.Msg.alert('Login failed', 'Please provide your userid and password.', Ext.emptyFn).doComponentLayout();
                                }
                                
                            }
                            else {
                                Ext.Msg.alert('Login failed', 'Please provide your userid and password.', Ext.emptyFn).doComponentLayout();
                            }
                        }
                        else 
                            if (userID == "") {
                                if ((checkConnection == 'Unknown connection') ||
                                (checkConnection == 'No network connection')) {
                                    if (PARAMS_STUDENT_ID == "") {
                                        Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                                            navigator.app.exitApp();
                                        }).doComponentLayout();
                                    }
                                    else {
                                        Ext.Msg.alert('Login failed', 'Please provide your userid.', Ext.emptyFn).doComponentLayout();
                                    }
                                    
                                }
                                else {
                                    Ext.Msg.alert('Login failed', 'Please provide your userid.', Ext.emptyFn).doComponentLayout();
                                }
                            }
                            else 
                                if (password == "") {
                                    if ((checkConnection == 'Unknown connection') ||
                                    (checkConnection == 'No network connection')) {
                                        if (PARAMS_STUDENT_ID == "") {
                                            Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                                                navigator.app.exitApp();
                                            }).doComponentLayout();
                                        }
                                        else {
                                            Ext.Msg.alert('Login failed', 'Please provide your password.', Ext.emptyFn).doComponentLayout();
                                        }
                                        
                                    }
                                    else {
                                        Ext.Msg.alert('Login failed', 'Please provide your password.', Ext.emptyFn).doComponentLayout();
                                    }
                                }
                                else {
                                
                                    if ((checkConnection == 'Unknown connection') ||
                                    (checkConnection == 'No network connection')) {
                                        var offlinePassword = encodeURIComponent(password);
                                        if ((PARAMS_STUDENT_ID == userID) &&
                                        (PARAMS_STUDENT_PASSWORD == offlinePassword)) {
                                            PARAMS_OFFLINEMODE = true;
                                            if (OFFLINESUPPORT) {
                                                SP.WebSSOPlugin.displayLandingPage();
                                            }
                                            else {
                                                Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                                                    navigator.app.exitApp();
                                                }).doComponentLayout();
                                            }
                                        }
                                        else 
                                            if (PARAMS_STUDENT_ID == "") {
                                                Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                                                    navigator.app.exitApp();
                                                }).doComponentLayout();
                                            }
                                            
                                            else {
                                                Ext.Msg.alert("Authentication failed.", "You have used an invalid user name or password.").doComponentLayout();
                                            }
                                        
                                    }
                                    else {
                                        PARAMS_OFFLINEMODE = false;
                                        // SP.WebSSOPlugin.checkWebSSO(userID,
                                        // password,
                                        // "",
                                        // "credentialsLogin");
                                        // STUDENT
                                        // ONLY
                                        // AUTHENTICATION
                                        if (AUTHENTICATION_STUDENTONLY == true) {
                                        
                                            if (userID.charAt(0) ==
                                            'p') {
                                                	try {
														var previousStoredUserID = SP.WebSSOPlugin.getUserIDStoredValue();
														if ((previousStoredUserID != userID) && (previousStoredUserID != "")) {
															var requestURL = "http://smobwebprd01.sf.sp.edu.sg/AlertWS/student/alert/RemoveDevice.jsp?" + PARAMS_DEVICE_TOKEN;
															
															Ext.Ajax.request({
																url: requestURL,
																method: "GET",
																success: function(response, opts){
																		console.log("Device Token Successfully removed from server");
																},
																failure: function(){
																	console.log("Failed removal of Device Token from server");
																}
																
															});
														}
													}
													catch(err){}
                                                PARAMS_OFFLINEMODE = false;
                                                SP.WebSSOPlugin.checkWebSSO(userID, password, "", "credentialsLogin");
                                            }
                                            else {
                                                Ext.Msg.alert('Login failed', 'This app is only for students.', Ext.emptyFn).doComponentLayout();
                                            }
                                            
                                        // ////////
                                        // STAFF
                                        // ONLY
                                        // AUTHENTICATION
                                        }
                                        else 
                                            if (AUTHENTICATION_STAFFONLY == true) {
                                            
                                                if (userID.charAt(0) ==
                                                's') {
                                                    try {
														var previousStoredUserID = SP.WebSSOPlugin.getUserIDStoredValue();
														if ((previousStoredUserID != userID) && (previousStoredUserID != "")) {
															var requestURL = "http://smobwebprd01.sf.sp.edu.sg/AlertWS/student/alert/RemoveDevice.jsp?" + PARAMS_DEVICE_TOKEN;
															
															Ext.Ajax.request({
																url: requestURL,
																method: "GET",
																success: function(response, opts){
																		console.log("Device Token Successfully removed from server");
																},
																failure: function(){
																	console.log("Failed removal of Device Token from server");
																}
																
															});
														}
													}
													catch(err){}
													
                                                    PARAMS_OFFLINEMODE = false;
                                                    SP.WebSSOPlugin.checkWebSSO(userID, password, "", "credentialsLogin");
                                                }
                                                else {
                                                    Ext.Msg.alert('Login failed', 'This app is only for staffs.', Ext.emptyFn).doComponentLayout();
                                                }
                                                
                                            // ////////
                                            // STUDENT/STAFF
                                            // AUTHENTICATION
                                            }
                                            else 
                                                if (AUTHENTICATION_STUDENT_AND_STAFF == true) {
														try {
															var previousStoredUserID = SP.WebSSOPlugin.getUserIDStoredValue();
															if ((previousStoredUserID != userID) && (previousStoredUserID != "")) {
																var requestURL = "http://smobwebprd01.sf.sp.edu.sg/AlertWS/student/alert/RemoveDevice.jsp?" + PARAMS_DEVICE_TOKEN;
																
																Ext.Ajax.request({
																	url: requestURL,
																	method: "GET",
																	success: function(response, opts){
																			console.log("Device Token Successfully removed from server");
																	},
																	failure: function(){
																		console.log("Failed removal of Device Token from server");
																	}
																	
																});
															}
														}
														catch(err){}
                                                    
                                                    
                                                    SP.WebSSOPlugin.checkWebSSO(userID, password, "", "credentialsLogin");
                                                // ////////
                                                // GENERAL
                                                // AUTHENTICATION
                                                }
                                                else {
													try {
														var previousStoredUserID = SP.WebSSOPlugin.getUserIDStoredValue();
														if ((previousStoredUserID != userID) && (previousStoredUserID != "")) {
															var requestURL = "http://smobwebprd01.sf.sp.edu.sg/AlertWS/student/alert/RemoveDevice.jsp?" + PARAMS_DEVICE_TOKEN;
															
															Ext.Ajax.request({
																url: requestURL,
																method: "GET",
																success: function(response, opts){
																		console.log("Device Token Successfully removed from server");
																},
																failure: function(){
																	console.log("Failed removal of Device Token from server");
																}
																
															});
														}
													}
													catch(err){}
													
                                                    PARAMS_OFFLINEMODE = false;
                                                    SP.WebSSOPlugin.checkWebSSO(userID, password, "", "credentialsLogin");
                                                }
                                        
                                    }
                                }
                        
                    }
                }]
            
            }, {
                flex: 2.5
            }]
    }
});


