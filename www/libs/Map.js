/*****************************************************
 * Variable Declarations
 *****************************************************/
var isDiagnosticEnabled = false;
var status_LIBRARY_MP010 = true;
var status_LIBRARY_MP011 = true;
var status_LIBRARY_MP020 = true;
var status_LIBRARY_MP021 = true;
var status_LIBRARY_MP022 = true;
var status_LIBRARY_MP023 = true;
var status_LIBRARY_MP024 = true;
var status_LIBRARY_MP025 = true;
var status_LIBRARY_MP030 = true;
var status_LIBRARY_MP040 = true;
var status_LIBRARY_MP041 = true;
var status_LIBRARY_MP050 = true;
var status_LIBRARY_MP051 = true;
var status_LIBRARY_MP060 = true;
var isLogEnabled = true;
var SPMap = null;
var SPGraphic = null;

/******************************************************
 * Map Library
 *
 * Functions:
 *      SP.Map.addMaptoPage()
 *      SP.Map.drawRoute(jsonPointCoordinates, routeColor)
 *      SP.Map.addMarker(markerGraphics, geometryPointX, geometryPointY, markerName, markerDescription)
 *
 ******************************************************/
dojo.require("esri.map");

var SP = SP || {};

SP.Map = {
    /******************************************************
     * Map Variable Declarations
     ******************************************************/
    //map: null, graphic: null,
    /* end of Map Variable Declarations */
    
    /*****************************************************
     * Map Initialization
     *****************************************************/
    init: function(){
        try {
            if (isLogEnabled) {
                console.log("Map Name: " + DIV_MAP_NAME);
            }
            console.log("LOG: init");
            SPMap = new esri.Map(DIV_MAP_NAME);
            dojo.connect(SPMap, "onLoad", SP.Map.initFunc);
            var tiledMapServiceLayer = SP.Map.setTiledMapServiceLayer(MAP_SERVER01);
            /** 
             * add the MapService layer to the map
             */
            SPMap.addLayer(tiledMapServiceLayer);
            if (isLogEnabled) {
                console.log("LOG: Map Initialization successful.");
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP010 = true;
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Map Initialization failed.");
                console.log(err.description);
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP011 = true;
            }
        }
    },
    /* end of init */
    
    /*****************************************************
     * Map Geolocation Current Position
     * @param {esri.Map} map ESRI Map
     *****************************************************/
    initFunc: function(map){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(SP.Map.zoomToLocation, SP.Map.locationError);
            navigator.geolocation.watchPosition(SP.Map.showLocation, SP.Map.locationError);
        }
        if (isDiagnosticEnabled) {
            /** 
             * Resize the info window
             */
            SPMap.infoWindow.resize(250, 100);
            /* Synergy Marker */
            var markerGraphics01 = "diagnostic/images/info_red.png";
            var markerName01 = "The Synergy";
            var markerDescription01 = "1 International Business Park (S)609917";
            SP.Map.addMarker(markerGraphics01, 11549218.329590, 147969.992905134, markerName01, markerDescription01);
            /* iHub Marker */
            var markerGraphics02 = "diagnostic/images/info_green.png";
            var markerName02 = "iHub";
            var markerDescription02 = "9 Jurong Town Hall Road Singapore (S)609431";
            SP.Map.addMarker(markerGraphics02, 11548554.329596, 147939.992905134, markerName02, markerDescription02);
            /* Route */
            var routeColor = new dojo.Color([102, 0, 51]);
            var jsonPointCoordinates = {
                "paths": [[[11549218.329590, 147969.992905134], [11548554.329596, 147939.992905134]]],
                "spatialReference": {
                    "wkid": 102100
                }
            };
            SP.Map.drawRoute(jsonPointCoordinates, routeColor);
            /* status_LIBRARY_MP022  - image parameter missing */
            var markerName04 = "iHub";
            var markerDescription04 = "9 Jurong Town Hall Road Singapore (S)609431";
            SP.Map.addMarker(11549218.329590, 150969.992905134, markerName04, markerDescription04);
            /* status_LIBRARY_MP023  - Add Marker - passing empty string as markerName */
            var markerGraphics05 = "diagnostic/images/info_red.png";
            var markerName05 = "";
            var markerDescription05 = "9 Jurong Town Hall Road Singapore (S)609431";
            SP.Map.addMarker(markerGraphics05, 11549318.329590, 148969.992905134, markerName05, markerDescription05);
            /* status_LIBRARY_MP025  - Add Marker - passing NULL locations */
            var markerGraphics06 = "diagnostic/images/info_green.png";
            var markerName06 = "iHub";
            var markerDescription06 = "9 Jurong Town Hall Road Singapore (S)609431";
            SP.Map.addMarker(markerGraphics06, "", "", markerName06, markerDescription06);
        }
    },
    /* end of initFunc */
    
    /*****************************************************
     * Add the Map to the page
     *****************************************************/
    addMaptoPage: function(){
        dojo.addOnLoad(SP.Map.init);
    },
    /* end of AddMaptoPage */
    
    /*****************************************************
     * Draw the Route Path from one location to another
     * based on a series of geometric points
     * @param {Json} jsonPointCoordinates Route coordinates
     * @param {dojo.Color} routeColor Route color (ex. dojo.Color([102,0,51]))
     *****************************************************/
    drawRoute: function(jsonPointCoordinates, routeColor){
        try {
            if (isDiagnosticEnabled) {
                if (routeColor != null) {
                    status_LIBRARY_MP051 = true;
                }
            }
            /** 
             * Define the line symbol
             */
            var polylineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, routeColor, 4);
            var runningRoute = new esri.geometry.Polyline(jsonPointCoordinates);
            /** 
             * add the defined graphic to the map
             */
            SPMap.graphics.add(new esri.Graphic(runningRoute, polylineSymbol));
            if (isLogEnabled) {
                console.log("LOG: DrawRoute successful.");
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP050 = true;
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. DrawRoute failed.");
                console.log(err.description);
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP050 = false;
            }
        }
    },
    /* end of drawRoute */
    
    /*****************************************************
     * Add a Marker to the map
     * @param {Image} markerGraphics Image to be used in the marker
     * @param {Float} geometryPointX X-coordinate
     * @param {Float} geometryPointY Y-coordinate
     * @param {String} markerName Name of the marker
     * @param {String} markerDescription Description of the marker
     *****************************************************/
    addMarker: function(markerGraphics, geometryPointX, geometryPointY, markerName, markerDescription){
        try {
            if (markerDescription == null) {
                if (isDiagnosticEnabled) {
                    status_LIBRARY_MP022 = true;
                }
                else {
                    alert("Error. Add Marker missing parameter.");
                }
            }
            if (isDiagnosticEnabled) {
                if (markerName == "") {
                    status_LIBRARY_MP023 = true;
                }
                if ((typeof(geometryPointX) != 'number') || (typeof(geometryPointY) != 'number')) {
                    status_LIBRARY_MP025 = true;
                }
            }
            /** 
             * Define the Picture Marker Symbol
             */
            var infoSymbol = new esri.symbol.PictureMarkerSymbol(markerGraphics, 30, 30);
            var infoTemplate = new esri.InfoTemplate("${Name}", "${Description}<br />");
            /** 
             * Define the point locaiton with X and Y geometry points
             */
            var pointLocation = new esri.Graphic({
                "geometry": {
                    "x": geometryPointX,
                    "y": geometryPointY,
                    "spatialReference": {
                        "wkid": 102100
                    }
                },
                "attributes": {
                    "Name": markerName,
                    "Description": markerDescription
                }
            });
            /** 
             * set the Symbol for point location
             */
            pointLocation.setSymbol(infoSymbol);
            pointLocation.setInfoTemplate(infoTemplate);
            /** 
             * Add the point location to map
             */
            SPMap.graphics.add(pointLocation);
            if (isLogEnabled) {
                console.log("LOG: AddMarker successful.");
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP020 = true;
                status_LIBRARY_MP021 = true;
                status_LIBRARY_MP060 = true;
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. AddMarker failed.");
                console.log(err.description);
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP020 = false;
            }
        }
    },
    /* end of AddMarker */
    
    /*****************************************************
     * Function that handles the orientation change
     *****************************************************/
    orientationChanged: function(){
        if (SPMap) {
            if (isLogEnabled) {
                console.log("LOG: Map exist");
            }
            /** 
             * handle map positon changed
             */
            SPMap.reposition();
            SPMap.resize();
        }
        else {
            if (isLogEnabled) {
                console.log("LOG: Map not exist");
            }
            
        }
    },
    /* end of orientationChanged */
    
    /*****************************************************
     * Sets the Tiled Map Service Layer
     * @param {String} mapServer Map Server url link
     * @return {ArcGISTiledMapServiceLayer} esri.layers.ArcGISTiledMapServiceLayer
     *****************************************************/
    setTiledMapServiceLayer: function(mapServer){
        return new esri.layers.ArcGISTiledMapServiceLayer(MAP_SERVER01);
    },
    /* end of setTiledMapServiceLayer */
    
    /*****************************************************
     * Zooms the map depending on the assigned level.
     * @param {Point/Coordinate} location Current Location
     *****************************************************/
    zoomToLocation: function(location){
        try {
            var pt = SP.Map.getGeometryPoint(location);
            /** 
             * Centers the map and zoom to the defined level
             */
            SPMap.centerAndZoom(pt, LOCATION_ZOOM_LEVEL);
            if (isLogEnabled) {
                console.log("LOG: zoomToLocation successful.");
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP030 = true;
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. zoomToLocation failed.");
                console.log(err.description);
            }
        }
    },
    /* end of zoomToLocation */
    
    /*****************************************************
     * Set the Picture of the location marker
     * @param {Image} markerGraphic Image to be used in the marker
     * @param {Integer} markerWidth  Width of the marker
     * @param {Integer} markerHeight Height of the marker
     *****************************************************/
    setLocationMarkerPicture: function(markerGraphic, markerWidth, markerHeight){
        return new esri.symbol.PictureMarkerSymbol(markerGraphic, markerWidth, markerHeight);
    },
    /* end of setLocationarkerPicture */
    
    /*****************************************************
     * Get the geometry Point
     * @param {Location} location Current Location
     * @return {esri.geometry.geographicToWebMercator} currentLocationPoint Current Location Point
     *****************************************************/
    getGeometryPoint: function(location){
        var currentLocationPoint = esri.geometry.geographicToWebMercator(new esri.geometry.Point(location.coords.longitude, location.coords.latitude));
        return currentLocationPoint;
    },
    /* getGeometryPoint */
    
    
    /*****************************************************
     * Handles different type of location errors
     * @param {error} error
     * @return {Alert} Alert Error Notification
     *****************************************************/
    locationError: function(error){
        if (isLogEnabled) {
            console.log("LOG: Error Code: " + error.code);
        }
        switch (error.code) {
            case error.PERMISSION_DENIED:
                if (!isDiagnosticEnabled) {
                    /** 
                 * Permission Denied while getting the location
                 */
                    alert(ERROR_LOCATION_PERMISSION_DENIED);
                }
                break;
            case error.POSITION_UNAVAILABLE:
                if (!isDiagnosticEnabled) {
                    /** 
                 * Position not available while getting the location
                 */
                    alert(ERROR_LOCATION_POSITION_UNAVAILABLE);
                }
                break;
            case error.TIMEOUT:
                if (!isDiagnosticEnabled) {
                    /** 
                 * Connection Timeout while getting the location
                 */
                    alert(ERROR_LOCATION_TIMEOUT);
                }
                break;
            default:
                if (!isDiagnosticEnabled) {
                    alert(ERROR_LOCATION_OTHER);
                }
                break;
        }
    },
    /* end of location error */
    
    /*****************************************************
     * Sets the Location graphic on the current point
     * @param {esri.geometry.geographicToWebMercator} currentPoint Current Geometry Point
     * @param {esri.symbol.PictureMarkerSymbol} symbol Picture Marker symbol
     * @return {esri.Graphic} esri.Graphic Location Graphic
     *****************************************************/
    setLocationGraphic: function(currentPoint, symbol){
        return new esri.Graphic(currentPoint, symbol);
    },
    /* end of setLocationGraphic */
    
    /*****************************************************
     * Put a marker on the map based on the user's current location
     * @param {Point/Coordinate} location Current Location
     *****************************************************/
    showLocation: function(location){
    
        try {
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP024 = true;
                status_LIBRARY_MP030 = true;
                status_LIBRARY_MP040 = true;
                status_LIBRARY_MP041 = true;
            }
            /** zoom to the users location and add a graphic */
            var pt = SP.Map.getGeometryPoint(location);
            //var graphic;
            
            
            if (!graphic) {
                if (isLogEnabled) {
                    console.log("LOG: Graphic not exist");
                }
                if (isDiagnosticEnabled) {
                    status_LIBRARY_MP024 = true;
                }
                /** 
                 * Set the Map current location Marker
                 */
                var symbol = SP.Map.setLocationMarkerPicture(LOCATION_MARKER_GRAPHIC, LOCATION_MARKER_GRAPHIC_WIDTH, LOCATION_MARKER_GRAPHIC_HEIGHT);
                SPMap = SP.Map.setLocationGraphic(pt, symbol);
                SPMap.graphics.add(SPGraphic);
            }
            else {
                /** move the graphic if it already exists */
                if (isLogEnabled) {
                    console.log("LOG: Graphic exist");
                }
                SPMap.graphic.setGeometry(pt);
            }
            if (isLogEnabled) {
                console.log("LOG: showLocation successful.");
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP040 = true;
            }
            SPMap.centerAt(pt);
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. showLocation failed.");
                console.log(err.description);
            }
            if (isDiagnosticEnabled) {
                status_LIBRARY_MP040 = true;
            }
        }
    },
    /* end of showLocation */
    
    /*****************************************************
     * Check Map Status
     *
     *****************************************************/
    checkMap: function(){
        var mapDivContent = document.getElementById("map").innerHTML;
        if (mapDivContent.indexOf(MAP_SERVER01) == -1) {
            status_LIBRARY_MP010 = false;
            status_LIBRARY_MP011 = false;
            status_LIBRARY_MP020 = false;
            status_LIBRARY_MP021 = false;
            status_LIBRARY_MP022 = false;
            status_LIBRARY_MP023 = false;
            status_LIBRARY_MP024 = false;
            status_LIBRARY_MP025 = false;
            status_LIBRARY_MP030 = false;
            status_LIBRARY_MP040 = false;
            status_LIBRARY_MP041 = false;
            status_LIBRARY_MP050 = false;
            status_LIBRARY_MP051 = false;
            status_LIBRARY_MP060 = false;
        }
        checkMapCounter++;
    }
    /* end of checkmap */
};


/*Sample Usage


 //Synergy Marker


 var markerGraphics01 = "library/images/info_red.png";


 var markerName01 = "The Synergy";


 var markerDescription01 = "1 International Business Park (S)609917";


 SP.Map.addMarker(markerGraphics01,11549218.329590,147969.992905134, markerName01, markerDescription01);


 


 //iHub Marker


 var markerGraphics02 = "library/images/info_green.png";


 var markerName02 = "iHub";


 var markerDescription02 = "9 Jurong Town Hall Road Singapore (S)609431";


 SP.Map.addMarker(markerGraphics02,11548554.329596,147939.992905134, markerName02, markerDescription02);


 


 //Route


 var routeColor = new dojo.Color([102,0,51]);


 var jsonPointCoordinates = {"paths":[[[11549218.329590,147969.992905134],[11548554.329596,147939.992905134]]],"spatialReference":{"wkid":102100}};


 SP.Map.drawRoute(jsonPointCoordinates, routeColor);


 */



