/******************************************************
 * Variable Declarations
 ******************************************************/
var isDiagnosticEnabled = false;
isLogEnabled = true;

/******************************************************
 * HttpRequest Library
 *
 * Functions:
 *   SP.HttpRequest.request(url, method, params, responseType, storeName, storeModel, storeRoot, storeRecord, listName, listTitle, itemTpl, fullscreen)
 *
 ******************************************************/
var SP = SP || {};

SP.HttpRequest = {
    /******************************************************
     * Variable Declarations
     ******************************************************/
    httpUrl: "",
    httpResponseType: "",
    httpMethod: "",
    httpParams: "",
    httpResponse: "",
    httpResponseStatus: "",
    httpStoreModel: "",
    httpStoreName: "",
    httpStoreRoot: "",
    httpStoreRecord: "",
    httpListName: "",
    httpListTitle: "",
    httpListItemTpl: "",
    httpListFullscreen: "",
    /******************************************************
     * HttpRequest function with integrated WeBSSSO
     * @param {String} url Url link
     * @param {String} method HTTP Request Method (ex. 'GET', 'POST')
     * @param {String} responseType HTTP Response type (ex. 'xml', 'json', text')
     * @param {String} storeName Name of the store to be used
     * @param {String} storeModel Name of the model the store will use
     * @param {String} listName Name of the list
     *
     ******************************************************/
    request: function(url, method, params, responseType, storeName, storeModel, storeRoot, storeRecord, listName, listTitle, itemTpl, fullscreen){
        var connectionStatus = SP.HttpRequest.checkConnection();
        SP.HttpRequest.httpUrl = url;
        SP.HttpRequest.httpResponseType = responseType;
        SP.HttpRequest.httpMethod = method;
        SP.HttpRequest.httpParams = params;
        SP.HttpRequest.httpResponse = "";
        SP.HttpRequest.httpStoreModel = storeModel;
        SP.HttpRequest.httpStoreName = storeName;
        SP.HttpRequest.httpStoreRoot = storeRoot;
        SP.HttpRequest.httpStoreRecord = storeRecord;
        SP.HttpRequest.httpListName = listName;
        SP.HttpRequest.httpListTitle = listTitle;
        SP.HttpRequest.httpListItemTpl = itemTpl;
        SP.HttpRequest.httpListFullscreen = fullscreen;
        if (isLogEnabled) {
            console.log("LOG: httpUrl: " + SP.HttpRequest.httpUrl);
            console.log("LOG: httpResponseType: " + SP.HttpRequest.httpResponseType);
            console.log("LOG: httpMethod: " + SP.HttpRequest.httpMethod);
            console.log("LOG: httpParams: " + SP.HttpRequest.httpParams);
            console.log("LOG: httpResponse: " + SP.HttpRequest.httpResponse);
            console.log("LOG: httpStoreModel: " + SP.HttpRequest.httpStoreModel);
            console.log("LOG: httpStoreName: " + SP.HttpRequest.httpStoreName);
            console.log("LOG: httpStoreRoot: " + SP.HttpRequest.httpStoreRoot);
            console.log("LOG: httpStoreRecord: " + SP.HttpRequest.httpStoreRecord);
            console.log("LOG: httpListName : " + SP.HttpRequest.httpListName);
            console.log("LOG: httpListTitle : " + SP.HttpRequest.httpListTitle);
            console.log("LOG: httpListItemTpl : " + SP.HttpRequest.httpListItemTpl);
            console.log("LOG: httpListFullscreen : " + SP.HttpRequest.httpListFullscreen);
        }
        /** 
         * No network connection found
         */
        if ((connectionStatus == 'Unknown connection') || (connectionStatus == 'No network connection')) {
            if (isLogEnabled) {
                console.log("LOG: Error: " + connectionStatus);
            }
            if (!isDiagnosticEnabled) {
                alert("Error: No Connection");
            }
        }
        else {
            /** 
             * Network connection found
             */
            if (isLogEnabled) {
                console.log("LOG: Connection detected");
            }
            SP.HttpRequest.httpKeychain();
        }
    },
    /* end of request */
    
    /******************************************************
     * Check Internet, Wifi, and Cellular Connection
     * @return {String} states[networkState] Network State
     *
     ******************************************************/
    checkConnection: function(){
        var networkState = navigator.network.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        if (isLogEnabled) {
            console.log("LOG: Network Type:" + states[networkState]);
        }
        return states[networkState];
    },
    /* end of checkConnection */
    
    /******************************************************
     * Get the Keychain Values
     *
     ******************************************************/
    httpKeychain: function(){
        try {
            /** 
             * Get values from keychain/AccountAuthenticator
             */
            SP.WebSSOPlugin.getUserID();
            SP.WebSSOPlugin.getPassword();
            SP.WebSSOPlugin.getCookie();
            
            setTimeout("SP.HttpRequest.httpKeychainTimeout()", 1000);
            if (isLogEnabled) {
                console.log("LOG: Get Keychain Values successful.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Get Keychain Values failed.");
            }
        }
    },
    /* end of httpKeychain */
    
    /******************************************************
     * Get the HTTP Request Response
     * @return {String/XML/JSON} httpResponse Http Response
     *
     ******************************************************/
    getHttpResponse: function(){
        if (isLogEnabled) {
            console.log("LOG: getHttpResponse: " + SP.HttpRequest.httpResponse);
        }
        return SP.HttpRequest.httpResponse;
    },
    /* end of getHttpResponse */
    
    /******************************************************
     * Set the HTTP Request Response
     * @param {String/XML/JSON} response Http Response
     *
     ******************************************************/
    setHttpResponse: function(response){
        if (isLogEnabled) {
            console.log("LOG: setHttpResponse: " + response);
        }
        SP.HttpRequest.httpResponse = response;
    },
    /* end of setHttpResponse */
    
    getHttpResponseTimeout: function(){
        return SP.HttpRequest.httpResponse;
    },
    /* end of getHttpResponseTimeout */
    
    /******************************************************
     * Get the HTTP Response Type
     * @return {String/XML/JSON} httpResponseType Http Request Type
     *
     ******************************************************/
    getHttpResponseType: function(){
        return SP.HttpRequest.httpResponseType;
    },
    /* end of getHttpResponseType */
    
    /******************************************************
     * Get the HTTP Request Type
     * @return {String/XML/JSON} httpResponseType Http Request Type
     *
     ******************************************************/
    getHttpRequestType: function(){
        return SP.HttpRequest.httpResponseType;
    },
    /* end of getHttpRequestType */
    
    /******************************************************
     * Check the keychain values (UserID, Password, Cookie) and perform corresponding functions
     *
     ******************************************************/
    httpKeychainTimeout: function(){
        /** 
         * Get the stores values
         */
        var keychainUserID = SP.WebSSOPlugin.getUserIDStoredValue();
        var keychainPassword = SP.WebSSOPlugin.getPasswordStoredValue();
        var keychainCookie = SP.WebSSOPlugin.getCookieStoredValue();
        
        if (isLogEnabled) {
            console.log("LOG: keychainUserID : " + keychainUserID);
            console.log("LOG: keychainPassword : " + keychainPassword);
            console.log("LOG: keychainCookie : " + keychainCookie);
        }
        /** 
         * No stored Cookie found in keychain/file
         */
        if (keychainCookie.length == 0) {
            /** 
             * Cookies Not Found, No UserID and No Password found in keychain/file
             */
            if ((keychainUserID.length == 0) || (keychainPassword.length == 0)) {
                if (isLogEnabled) {
                    console.log("LOG: Error. Credentials not Found");
                }
                if (!isDiagnosticEnabled) {
                    alert("No credentials");
                }
                if (isDiagnosticEnabled) {
                    /** 
                     * Preform WebSSO Http request
                     */
                    SP.HttpRequest.checkHttpRequestWebSSO("", "", "", "credentialsLogin");
                }
                else {
                    /** 
                     * Display the Login Form
                     */
                    SP.HttpRequest.displayHttpRequestLoginForm();
                }
            }
            /** 
             * Cookies Not Found, UserID and Password found in keychain/file
             */
            else {
                if (isLogEnabled) {
                    console.log("LOG: User Credentials Found");
                }
                if (!isDiagnosticEnabled) {
                    alert("User credentials found in keychain");
                }
                else {
                    /** 
                     * Perform the WebSSO Http Request
                     */
                    SP.HttpRequest.checkHttpRequestWebSSO(decodeURIComponent(keychainUserID), decodeURIComponent(keychainPassword), "", "credentialsLogin");
                }
            }
        }
        else {
            /** 
             * Cookies Found in keychain/file
             */
            if (!isDiagnosticEnabled) {
                alert("With Cookie in Keychain");
            }
            if (isLogEnabled) {
                console.log("LOG: With Cookie in Keychain");
            }
            SP.HttpRequest.httpResponseStatus = "With Cookies";
            if (isLogEnabled) {
                console.log("LOG: Response Type:" + SP.HttpRequest.httpResponseType);
            }
            /** 
             * Handle XML response
             */
            if (SP.HttpRequest.httpResponseType == 'xml') {
                if (isLogEnabled) {
                    console.log("LOG: Process XML");
                }
                /** 
                 * Creating store and load XML after successful login
                 */
                if (isLogEnabled) {
                    console.log("LOG: Creating store and load XML after successful login");
                }
                window[SP.HttpRequest.httpStoreName] = new Ext.data.Store({
                    model: SP.HttpRequest.httpStoreModel,
                    method: 'GET',
                    autoLoad: true,
                    proxy: {
                        type: 'ajax',
                        url: SP.HttpRequest.httpUrl,
                        reader: {
                            type: 'xml',
                            root: SP.HttpRequest.httpStoreRoot,
                            record: SP.HttpRequest.httpStoreRecord
                        }
                    }
                });
                window[SP.HttpRequest.httpStoreName].getProxy().extraParams.Cookie = keychainCookie;
                if (isDiagnosticEnabled) {
                    SP.HttpRequest.httpListFullscreen = false;
                }
                /** 
                 * Creating list and load store after successful login
                 */
                if (isLogEnabled) {
                    console.log("LOG: Creating list and load store after successful login");
                }
                window[SP.HttpRequest.httpListName] = new Ext.List({
                    fullscreen: SP.HttpRequest.httpListFullscreen,
                    title: SP.HttpRequest.httpListTitle,
                    itemTpl: SP.HttpRequest.httpListItemTpl,
                    store: window[SP.HttpRequest.httpStoreName]
                });
            }
            /** 
             * Handle JSON response
             */
            else 
                if (SP.HttpRequest.httpResponseType == 'json') {
                    if (isLogEnabled) {
                        console.log("LOG: Process JSON");
                    }
                    /** 
                 * Creating store and load JSON after successful login
                 */
                    if (isLogEnabled) {
                        console.log("LOG: Creating store and load JSON after successful login");
                    }
                    window[SP.HttpRequest.httpStoreName] = new Ext.data.Store({
                        model: SP.HttpRequest.httpStoreModel,
                        method: 'GET',
                        autoLoad: true,
                        proxy: {
                            type: 'ajax',
                            url: SP.HttpRequest.httpUrl,
                            reader: {
                                type: 'json',
                                root: SP.HttpRequest.httpStoreRoot,
                                record: SP.HttpRequest.httpStoreRecord
                            }
                        }
                    });
                    /** 
                 * Set Cookie
                 */
                    window[SP.HttpRequest.httpStoreName].getProxy().extraParams.Cookie = keychainCookie;
                    if (isDiagnosticEnabled) {
                        SP.HttpRequest.httpListFullscreen = false;
                    }
                    /** 
                 * Creating List and load store after successful login
                 */
                    if (isLogEnabled) {
                        console.log("LOG: Creating List and load store after successful login");
                    }
                    window[SP.HttpRequest.httpListName] = new Ext.List({
                        fullscreen: SP.HttpRequest.httpListFullscreen,
                        title: SP.HttpRequest.httpListTitle,
                        itemTpl: SP.HttpRequest.httpListItemTpl,
                        store: window[SP.HttpRequest.httpStoreName]
                    });
                }
        }
    },
    /* end of httpKeychainTimeout */
    
    /*****************************************************
     * Perform WebSSO Login using credentials/cookie
     * @param {String} userID UserID
     * @param {String} password Password
     * @param {String} cookie Cookie
     * @param {String} type Type of request (thru credentials/cookies)
     *
     *****************************************************/
    checkHttpRequestWebSSO: function(userID, password, cookie, type){
        var userIDEncoded = encodeURIComponent(userID);
        var passwordEncoded = encodeURIComponent(password);
        SP.WebSSOPlugin.formPost(["WebSSO", userIDEncoded, passwordEncoded, cookie, type], function(result){
            if (isLogEnabled) {
                console.log("LOG: TYPE: " + type);
                console.log("LOG: result: " + result);
            }
            if (type == "credentialsLogin") {
                if (result.length >= 14) {
                    /** 
                     * Successful WebSSO Login using credentials
                     */
                    if (result.indexOf("PD-H-SESSION-ID") != -1) {
                        /** 
                         * Encryption and Saving of credentials/cookies after success login
                         */
                        SP.WebSSOPlugin.saveUserID(userIDEncoded);
                        SP.WebSSOPlugin.savePassword(passwordEncoded);
                        SP.WebSSOPlugin.saveCookie(result);
                        if (isDiagnosticEnabled) {
                            /** 
                             * Display Landing Page
                             */
                            SP.HttpRequest.displayHttpRequestLandingPage();
                        }
                        else {
                            alert("Login successfully.");
                            /** 
                             * Display Landing Page
                             */
                            SP.HttpRequest.displayHttpRequestLandingPage();
                        }
                    }
                    else {
                        /** 
                         * Failed WebSSO Login using credentials
                         */
                        if (isLogEnabled) {
                            console.log("LOG: Failed WebSSO Login using credentials");
                        }
                        if (isDiagnosticEnabled) {
                        }
                        else {
                            alert("Login Failed");
                        }
                    }
                }
                else {
                    /** 
                     * Failed WebSSO Login using credentials
                     */
                    if (isDiagnosticEnabled) {
                    }
                    else {
                        alert("Login Failed");
                    }
                }
            }
            else 
                if (type == "cookieLogin") {
                    if (result.indexOf("TAM_OP=login&ERROR_CODE") != -1) {
                        /** 
                     * Failed WebSSO Login using cookies
                     */
                        alert("Login failed");
                        SP.HttpRequest.displayHttpRequestLoginForm();
                    }
                    /** 
                 * Successful WebSSO Login using cookies
                 */
                    else 
                        if (result.indexOf("TAM_OP=help") != -1) {
                            if (isDiagnosticEnabled) {
                                SP.HttpRequest.displayHttpRequestLandingPage();
                            }
                            else {
                                alert("Login successfully.");
                                SP.HttpRequest.displayHttpRequestLandingPage();
                            }
                        }
                }
        }, function(error){
            /** 
             * Failed WebSSO Login callback
             */
            alert("Error : \r\n" + error);
        });
    },
    /* end of checkHttpRequestWebSSO */
    
    /*****************************************************
     * Display the Login Form
     *
     *****************************************************/
    displayHttpRequestLoginForm: function(){
        var httpRequestformBase = {
            items: [{
                xtype: 'fieldset',
                title: 'Welcome to SPICE Web Application Access Authentication Service',
                instructions: '(Password is case-sensitive)',
                defaults: {
                    required: true,
                    labelAlign: 'left',
                    labelWidth: '40%'
                },
                items: [{
                    xtype: 'textfield',
                    name: 'userid',
                    id: 'userid',
                    label: 'User ID',
                    value: 'p0931368',
                    autoCapitalize: false,
                    useClearIcon: true
                }, {
                    xtype: 'passwordfield',
                    name: 'password',
                    id: 'password',
                    label: 'Password',
                    value: 'Welcome123@',
                    useClearIcon: true
                }]
            }],
            dockedItems: [{
                xtype: 'toolbar',
                title: 'SP Login',
                dock: 'top',
                items: [{
                    text: 'Cancel',
                    handler: function(){
                        form.reset();
                    }
                }, {
                    xtype: 'spacer'
                }, {
                    text: 'Submit',
                    handler: function(){
                        var userID = Ext.getCmp('userid').getValue();
                        var password = Ext.getCmp('password').getValue();
                        SP.HttpRequest.checkHttpRequestWebSSO(userID, password, "", "credentialsLogin");
                    }
                }]
            }]
        };
        if (Ext.is.Phone) {
            httpRequestformBase.fullscreen = true;
        }
        else {
            Ext.apply(httpRequestformBase, {
                autoRender: true,
                floating: true,
                modal: true,
                centered: true,
                hideOnMaskTap: false,
                height: 385,
                width: 480
            });
        }
        var httpRequestform = new Ext.form.FormPanel(httpRequestformBase);
        httpRequestform.show();
    },
    /* end of displayHttpRequestLoginForm */
    
    /*****************************************************
     * Display The Login Page
     *
     *****************************************************/
    displayHttpRequestLandingPage: function(){
        SP.HttpRequest.request(SP.HttpRequest.httpUrl, SP.HttpRequest.httpMethod, SP.HttpRequest.httpParams, SP.HttpRequest.httpResponseType, SP.HttpRequest.httpStoreName, SP.HttpRequest.httpStoreModel, SP.HttpRequest.httpStoreRoot, SP.HttpRequest.httpStoreRecord, SP.HttpRequest.httpListName, SP.HttpRequest.httpListTitle, SP.HttpRequest.httpListItemTpl, SP.HttpRequest.httpListFullscreen);
        httpRequestform.hide();
    }
    /* end of displayHttpRequestLandingPage */
};



