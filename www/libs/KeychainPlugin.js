/*******************************************************
 * Variable Declarations
 *******************************************************/
var isDiagnosticEnabled = false;
var isGetCredentialsTestStarted = false;
var isSetCredentialsTestStarted = false;
var isSetUserIDTestStarted = false;
var isSetPasswordTestStarted = false;
var isSetCookieTestStarted = false;
var userIDkeychainValue = "";
var passwordkeychainValue = "";
var cookiekeychainValue = "";


function getKeychain (key, servicename){
    var win = function(key, value) {
        
        if(key=="userid")
        {
            value =decodeURIComponent(value);
            setUserIDKeychainValue(value);
        }
        else if (key=="password")
        {
            value =decodeURIComponent(value);
            setPasswordKeychainValue(value);
        }
        else if (key=="cookie")
        {
            setCookieKeychainValue(value);
        }
    };
    var fail = function(key, error) {
        if(key=="userid")
        {
            setUserIDKeychainValue("");
        }
        else if (key=="password")
        {
            setPasswordKeychainValue("");
        }
        else if (key=="cookie")
        {
            setCookieKeychainValue("");
        }
    };
    cordova.exec(win, fail, "KeychainPlugin", "getForKey", [key, servicename]);
    
}



/*****************************************************
 * Get/Set Keychain Value
 *****************************************************/
function getUserIDKeychainValue()
{
    return userIDkeychainValue;
}

function setUserIDKeychainValue(value)
{
    userIDkeychainValue = value;
}

function getPasswordKeychainValue()
{
    return passwordkeychainValue;
}

function setPasswordKeychainValue(value)
{
    passwordkeychainValue = value;
}

function getCookieKeychainValue()
{
    return cookiekeychainValue;
}

function setCookieKeychainValue(value)
{
    cookiekeychainValue = value;
}


function setKeychain (key, value, servicename)
{

    if((key=="userid")||(key=="password"))
	{
		value = encodeURIComponent(value);
    }
	
    
    if(key=="cookie")
    {
        Ext.Ajax.defaultHeaders = {
            'Cookie': value
        }
    }
    
	var win = function(key) {
		console.log("setKeychain SUCCESS key: "+key);
	};
	var fail = function(key, error) {
		console.log("setKeychain ERROR key: "+key);
	};
	
    cordova.exec(win, fail, "KeychainPlugin", "setForKey", [key, value, servicename]);
}



function   removeKeychain (key, servicename){
    var win = function(key) {
        if(key=="userid")
        {
            setUserIDKeychainValue("");
        }
        else if (key=="password")
        {
            setPasswordKeychainValue("");
        }
        else if (key=="cookie")
        {
            setCookieKeychainValue("");
        }
    };
    var fail = function(key, error) {
        if(key=="userid")
        {
            setUserIDKeychainValue("");
        }
        else if (key=="password")
        {
            setPasswordKeychainValue("");
        }
        else if (key=="cookie")
        {
            setCookieKeychainValue("");
        }
    };
    
    cordova.exec(win, fail, "KeychainPlugin", "removeForKey", [key, servicename]);
    
}



/*****************************************************
 * Remove all Keychain values
 *****************************************************/
function removeAllStoredValues()
{
    removeKeychain("userid","spkeychain");
    removeKeychain("password","spkeychain");
    removeKeychain("cookie","spkeychain");
}


