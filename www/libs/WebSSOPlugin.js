/*******************************************************************************
 * Variable Declarations
 ******************************************************************************/
var isDiagnosticEnabled = false;
var WebSSOLandingPageDisplayed = false;
var WebSSOLoginFormDisplayed = false;
var WebSSOResult = "";
var WebSSORequestType = "";
var isFirstWebSSOLogin = true;
var WebSSODomain = "";
var isLogEnabled = true;
var userIDkeychainValue = "";
var passwordkeychainValue = "";
var cookiekeychainValue = "";
var encryptionKey = "";

var PARAMS_OFFLINE_USERID = "";
var PARAMS_STUDENT_PASSWORD = "";
var PARAMS_DEVICE_TOKEN = "";

var PARAMS_MAINTIMEOUT;
var PARAMS_MAINTIMEOUTRETRY; 
var PARAMS_MAINTIMEOUTCOMPLETED;
var PARAMS_TIMEOUTSTOPPED;


/*****************************************************
 * WebSSO Library
 *
 * Functions:
 *      SP.WebSSOPlugin.checkWebSSO(userID, password, cookie, type)
 *      SP.WebSSOPlugin.checkStoredValues()
 *      SP.WebSSOPlugin.getUserID();
 *      SP.WebSSOPlugin.getPassword();
 *      SP.WebSSOPlugin.getCookie();
 *      SP.WebSSOPlugin.getKey();
 *      SP.WebSSOPlugin.setWebSSODomain(domainString);
 *      SP.WebSSOPlugin.clearUserData();
 *
 *****************************************************/
var SP = SP || {};

SP.WebSSOPlugin = {
    /***************************************************************************
     * Declarations
     **************************************************************************/
    userID: "",
    /***************************************************************************
     * Method for WebSSO Login using credentials/cookie
     *
     * @param {Array}
     *            types Login inputs
     * @param {Function
     *            callback} success Success callback
     * @param {Function
     *            callback} fail Fail callback
     *
     **************************************************************************/
    formPost: function(types, success, fail){
        if (isLogEnabled) {
            console.log("LOG: FormPost");
        }
	 return cordova.exec(success, fail, "WebSSOPlugin", "formPost", types);
    }, /* end of formPost */
    /***************************************************************************
     * Http Request with WebSSO Authentication
     *
     * @param {Array}
     *            types Request inputs
     * @param {Function
     *            callback} success Success callback
     * @param {Function
     *            callback} fail Fail callback
     *
     **************************************************************************/
    httpRequest: function(types, success, fail){
        if (isLogEnabled) {
            console.log("LOG: WebSSO Http Request");
        }
         return cordova.exec(success, fail, "WebSSOPlugin", "httpRequest", types);
    },
    /*****************************************************
     * Perform WebSSO Login using credentials/cookie
     * @param {String} userID UserID
     * @param {String} password Password
     * @param {String} cookie Cookie
     * @param {String} type Type of request (thru credentials/cookies)
     *
     *****************************************************/
    checkWebSSO: function(userID, password, cookie, type){
        /***********************************************************************
         * URI Encode of credentials
         **********************************************************************/
        var userIDEncoded = encodeURIComponent(userID);
        var passwordEncoded = encodeURIComponent(password);
        WebSSOLandingPageDisplayed = false;
        WebSSOLoginFormDisplayed = false;
        WebSSOResult = "";
        WebSSORequestType = type;
        puid = userIDEncoded;
        SP.WebSSOPlugin.userID = userID;
        /***********************************************************************
         * Display Loading Mask
         **********************************************************************/
        if (!isDiagnosticEnabled) {
			Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Connecting...'
        });
   
           // Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Connecting...</div>', 'x-mask-loading', true);
		    if (isLogEnabled) {
                console.log("LOG: Connection Mask");
            }
        }
        
        PARAMS_MAINTIMEOUT = setTimeout(function(){
            if (PARAMS_TIMEOUTSTOPPED == false) {
         //       Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Retrying(1)...</div>', 'x-mask-loading', true);
               if (!isDiagnosticEnabled) {
			   	Ext.Viewport.setMasked({
			   		xtype: 'loadmask',
			   		message: 'Retrying(1)...'
			   	});
			   }
	        }
        }, 15000);
        
        PARAMS_MAINTIMEOUTRETRY = setTimeout(function(){
            if (PARAMS_TIMEOUTSTOPPED == false) {
        //        Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Retrying(2)...</div>', 'x-mask-loading', true);
                         if (!isDiagnosticEnabled) {
			   	Ext.Viewport.setMasked({
			   		xtype: 'loadmask',
			   		message: 'Retrying(1)...'
			   	});
			   }
		    }
        }, 30000);
        
        PARAMS_MAINTIMEOUTCOMPLETED = setTimeout(function(){
            window.clearTimeout(PARAMS_MAINTIMEOUT);
            window.clearTimeout(PARAMS_MAINTIMEOUTRETRY);
            window.clearTimeout(PARAMS_MAINTIMEOUTCOMPLETED);
         //   Ext.getBody().unmask();
		   if (!isDiagnosticEnabled) {
		   	Ext.Viewport.setMasked(false);
		   }
            if (PARAMS_TIMEOUTSTOPPED == false) {
                
                if (type == "cookieLogin") {
                    SP.WebSSOPlugin.displayLoginForm();
                }
				 Ext.Msg.alert("Connection Failed", "Connection Timeout");
               
            }
        }, 45000);
        
        /***********************************************************************
         * Process the Encryption Keys
         **********************************************************************/
        SP.WebSSOPlugin.processEncryptionKey(function(){});
        
        /***********************************************************************
         * WebSSO Form Post
         **********************************************************************/
        if (isLogEnabled) {
            console.log("LOG: Perform WebSSO FormPost");
        }
        SP.WebSSOPlugin.formPost(["WebSSO", userIDEncoded, passwordEncoded, cookie, type], function(result){
            WebSSOResult = result;
            console.log("TYPE: " + type);
            console.log(result);
            PARAMS_TIMEOUTSTOPPED = true;
            /***************************************************
             * Hides the Loading Mask
             **************************************************/
            if (!isDiagnosticEnabled) {
		   Ext.Viewport.setMasked(false);
            }
            if (type == "credentialsLogin") {
                if (result.length >= 14) {
                    /*******************************************
                     * Successful WebSSO Login using credentials
                     ******************************************/
                    if (result.indexOf("PD-H-SESSION-ID") != -1) {
                        window.clearTimeout(PARAMS_MAINTIMEOUT);
                        window.clearTimeout(PARAMS_MAINTIMEOUTRETRY);
                        window.clearTimeout(PARAMS_MAINTIMEOUTCOMPLETED);
                      //  Ext.getBody().unmask();
					    if (!isDiagnosticEnabled) {
							Ext.Viewport.setMasked(false);
						}
                        if (isLogEnabled) {
                            console.log("LOG: Successful WebSSO Authentication using credentials");
                        }
                        /***************************************
                         * Replace empty spaces with %20
                         **************************************/
                        result = result.replace(/\s/g, '');
                        result = result.replace("%20", "");
                        if (isLogEnabled) {
                            console.log("LOG: Replace empty spaces with %20 ");
                        }
                        /***************************************
                         * Checks whether the Domain is a test
                         * server or production server and
                         * handle the necessary adjustment in
                         * the domain name
                         **************************************/
                        if (WebSSODomain.indexOf("testsp") != -1) {
                            result = result.replace("Domain=.testsf.testsp.edu.sg", "Domain=" +
                            WebSSODomain);
                        }
                        else {
                            result = result.replace("Domain=.sp.edu.sg", "Domain=" + WebSSODomain);
                        }
                        /***************************************
                         * Sets the Session cookies to be used
                         * in all ajax request
                         **************************************/
                        if (isLogEnabled) {
                            console.log("LOG: Sets the Session cookies to be used in all ajax request");
                        }
                        // Ext.Ajax.defaultHeaders = {
                        // 'Cookie': result
                        // }
                        /***************************************
                         * Encryption and Saving of
                         * credentials/cookies after success
                         * login
                         **************************************/
                        SP.WebSSOPlugin.saveUserID(userIDEncoded);
                        SP.WebSSOPlugin.savePassword(passwordEncoded);
                        SP.WebSSOPlugin.saveCookie(result);
                        
                        PARAMS_OFFLINE_USERID = userIDEncoded;
                        if (isDiagnosticEnabled) {
                            WebSSOLandingPageDisplayed = true;
                            SP.WebSSOPlugin.displayLandingPage();
                        }
                        else {
                            SP.WebSSOPlugin.displayLandingPage();
                        }
                    }
                    /*******************************************
                     * Failed WebSSO Login using credentials
                     ******************************************/
                    else {
                        if (isLogEnabled) {
                            console.log("LOG: Failed WebSSO Login using credentials");
                        }
                        if (isDiagnosticEnabled) {
                        //    Ext.getBody().unmask();
						  if (!isDiagnosticEnabled) {
						  	Ext.Viewport.setMasked(false);
						  }
                            WebSSOLoginFormDisplayed = true;
                            SP.WebSSOPlugin.displayLoginForm();
                        }
                        else {
                            Ext.Msg.alert("Authentication failed.", "You have used an invalid user name or password.");
                        }
                    }
                }
                else 
                    if (isFirstWebSSOLogin == false) {
                        /*******************************************
                     * Saves the userID and password ito the
                     * keychain/AccountAuthenticator
                     ******************************************/
                        SP.WebSSOPlugin.saveUserID(userIDEncoded);
                        SP.WebSSOPlugin.savePassword(passwordEncoded);
                        SP.WebSSOPlugin.displayLandingPage();
                    }
                    else {
                        /*******************************************
                     * Failed WebSSO Login using credentials
                     ******************************************/
                        if (isDiagnosticEnabled) {
                            WebSSOLoginFormDisplayed = true;
                            SP.WebSSOPlugin.displayLoginForm();
                        }
                        else {
                            Ext.Msg.alert("Fail", "Login failed");
                            if (isLogEnabled) {
                                console.log("LOG: WebSSO Login Failed");
                            }
                        }
                    }
            }
            else 
                if (type == "cookieLogin") {
                    /***********************************************
                 * Failed WebSSO Login using cookies
                 **********************************************/
                    if (result.indexOf("TAM_OP=login&ERROR_CODE") != -1) {
                        if (isLogEnabled) {
                            console.log("LOG: Failed WebSSO Login using cookies");
                        }
                        /*******************************************
                     * Decodes the userid and password in
                     * keychain/AccountAuthenticator
                     ******************************************/
						var userID = decodeURIComponent(SP.WebSSOPlugin.getUserIDStoredValue());
                        var password = decodeURIComponent(SP.WebSSOPlugin.getPasswordStoredValue());
                        PARAMS_TIMEOUTSTOPPED = true;
                        if (isDiagnosticEnabled) {
                            WebSSOLoginFormDisplayed = true;
                        }
                        else {
 							SP.WebSSOPlugin.displayLoginForm();
							Ext.Msg.alert("Login Fail", "Cookie Expired.");
                        
                        }
                    }
                    /***********************************************
                 * Successful WebSSO Login using cookies
                 **********************************************/
                    else 
                        if (result.indexOf("TAM_OP=help") != -1) {
                            if (isLogEnabled) {
                                console.log("LOG: Successful WebSSO Login using cookies");
                            }
                            if (isDiagnosticEnabled) {
                                WebSSOLandingPageDisplayed = true;
                                SP.WebSSOPlugin.displayLandingPage();
                            }
                            else {
                                isFirstWebSSOLogin = false;
                                SP.WebSSOPlugin.displayLandingPage();
                            }
                        }
                }
        }, function(error){
            /***************************************************
             * Failed WebSSO Login callback
             **************************************************/
            if (isLogEnabled) {
                console.log("LOG: Failed WebSSO Login callback");
            }
            if (!isDiagnosticEnabled) {
                Ext.Msg.alert("Error", "Login failed.");
            }
        });
    }, /* end of checkWebSSO */
    /***************************************************************************
     * set WebSSO Domain
     *
     **************************************************************************/
    setWebSSODomain: function(domainString){
        if (isLogEnabled) {
            console.log("LOG: Set WebSSO Domain");
        }
        WebSSODomain = domainString;
		    return cordova.exec(null, null, "WebSSOPlugin", "setWebSSODomain", [domainString]);
    }, /* end of getWebSSOResult */
    /***************************************************************************
     * Gets the result after WebSSO Login
     *
     **************************************************************************/
    getWebSSOResult: function(){
        return WebSSOResult;
    }, /* end of getWebSSOResult */
    /***************************************************************************
     * Gets the WebSSORequestType
     *
     **************************************************************************/
    getWebSSORequestType: function(){
        return WebSSORequestType;
    }, /* end of getWebSSORequestType */
    /***************************************************************************
     * Display the Login Form
     *
     **************************************************************************/
    displayLoginForm: function(){
        if (isLogEnabled) {
            console.log("LOG: Display Login Form");
        }
        if (!isDiagnosticEnabled) {
            WebSSOloginPage();
        }
        else {
            /**
             * Login Panel (Overlay) for Test Suite
             */
			
            var overlayLoginFormPanel =  Ext.create('Ext.Panel', {
                id: 'testLoginFormPanel',
                floating: true,
                modal: true,
                centered: false,
                width: 320,
                height: 380,
                styleHtmlContent: true,
                scroll: 'vertical',
                cls: 'htmlcontent',
                                                    hideAnimation: {
                                                    type: 'popOut',
                                                    duration: 200,
                                                    easing: 'ease-out'
                                                    },
                                                    showAnimation: {
                                                    type: 'popIn',
                                                    duration: 200,
                                                    easing: 'ease-out'
                                                    },
                                                    hideOnMaskTap: true,
                items: [{
                    xtype: 'fieldset',
                    title: 'Welcome to SPICE Web Application Access Authentication Service',
                    instructions: '(Password is case-sensitive)',
                    defaults: {
                        required: true,
                        labelAlign: 'left',
                        labelWidth: '40%'
                    },
                        items: [{
                                xtype: 'toolbar',
                                title: 'SP Login',
                                docked: 'top',
                                items: [{
                                        text: 'Cancel',
                                        handler: function(){
                                        }
                                        }, {
                                        xtype: 'spacer'
                                        }, {
                                        text: 'Submit',
                                        handler: function(){
                                        
                                        }
                                        }]
                                },{
                        xtype: 'textfield',
                        name: 'userid',
                        id: 'userid',
                        label: 'User ID',
                        autoCapitalize: false,
                        useClearIcon: true
                    }, {
                        xtype: 'passwordfield',
                        name: 'password',
                        id: 'password',
                        label: 'Password',
                        useClearIcon: true
                    }]
                }]

            }); 
            
        }
        
    }, /* end of displayLoginForm */
    /***************************************************************************
     * Display The Login Page
     *
     **************************************************************************/
    displayLandingPage: function(){
        if (isLogEnabled) {
            console.log("LOG: Display Landing Page");
        }
        if (!isDiagnosticEnabled) {
            WebSSOlandingPage();
        }
        else {
            /**
             * Landing Page Panel (Overlay) for Test Suite
             */
			
            var overlayLandingPanel =  Ext.create('Ext.Panel', {
                id: 'testLandingPanel',
                floating: true,
                modal: true,
                centered: false,
                width: 320,
                height: 380,
                styleHtmlContent: true,
                scroll: 'vertical',
                cls: 'htmlcontent',
                                                  hideAnimation: {
                                                  type: 'popOut',
                                                  duration: 200,
                                                  easing: 'ease-out'
                                                  },
                                                  showAnimation: {
                                                  type: 'popIn',
                                                  duration: 200,
                                                  easing: 'ease-out'
                                                  },
                                                    hideOnMaskTap: true,
                items: [{
                    xtype: 'toolbar',
                    title: 'Welcome to SP Page',
                    docked: 'top'
                }]
            }); 
        }
        
    }, /* end of displayLandingPage */
    /***************************************************************************
     * Check if the Landing Page is Displayed after WebSSO Login
     *
     **************************************************************************/
    isWebSSOLandingPageDisplayed: function(){
        return WebSSOLandingPageDisplayed;
    },
    
    /***************************************************************************
     * Check if Login Form is Displayed after WebSSO Login
     *
     **************************************************************************/
    isWebSSOLoginFormPageDisplayed: function(){
        return WebSSOLoginFormDisplayed;
        
    },
    /***************************************************************************
     * Start WebSSO
     *
     **************************************************************************/
    startWebSSO: function(){
		/*****************************************************
        * ISSUE DESCRIPTION/FIX:
        * Some times Special characters are displaying in 
        * User Id field when the session expires or offline mode 
        * 
        * Fix the error by making sure that before checking the stored values,
        * processing of encryption keys should be finished first.
        * 
        *  
        * INCIDENT NUMBER: 
        * IN-SP-2013-0002
        * 
        * UPDATED BY:
        * Fritz Llanora
        * 
        * Date:
        * June 12, 2013
        * 
        *****************************************************/
        
        if ((device.platform == 'iPhone Simulator') ||
            (device.platform == 'iPhone') ||
            (device.platform == 'iPad Simulator') ||
            (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            if (!isDiagnosticEnabled) {
                SP.WebSSOPlugin.getUserID();
                SP.WebSSOPlugin.getPassword();
                SP.WebSSOPlugin.getCookie();
            }
            /**
             * Calls the checkStoresValues Timeout is used to provides time for the
             * keychain to process the values
             */
            setTimeout("SP.WebSSOPlugin.checkStoredValues()", 800);
        }
        else
            if (device.platform == 'Android') {
            
                   try {
                      SP.WebSSOPlugin.processEncryptionKey(function(){
                		if (isLogEnabled) {
                    console.log("LOG: Start WebSSO");
                 }
                if (!isDiagnosticEnabled) {
                    SP.WebSSOPlugin.getUserID();
                    SP.WebSSOPlugin.getPassword();
                    SP.WebSSOPlugin.getCookie();
                }
                /**
                 * Calls the checkStoresValues Timeout is used to provides time for the
                 * keychain to process the values
                 */
                setTimeout("SP.WebSSOPlugin.checkStoredValues()", 800);
                
                	});
                 }
                 catch (err) {
                 }

            }
    
    },
    
    /***************************************************************************
     * Check Stored Values
     *
     **************************************************************************/
    checkStoredValues: function(){
        if (isLogEnabled) {
            console.log("LOG: Check Stored Values");
        }
        var storedUserID = SP.WebSSOPlugin.getUserIDStoredValue();
        var storedPassword = SP.WebSSOPlugin.getPasswordStoredValue();
        var storedCookie = SP.WebSSOPlugin.getCookieStoredValue();
        PARAMS_STUDENT_ID = SP.WebSSOPlugin.getUserIDStoredValue();
        PARAMS_STUDENT_PASSWORD = SP.WebSSOPlugin.getPasswordStoredValue();
        PARAMS_OFFLINE_USERID = SP.WebSSOPlugin.getUserIDStoredValue();
 
      /***********************************************************************
         * No stored Cookie found in keychain/AccountAuthenticator
         **********************************************************************/
        if (storedCookie.length == 0) {
            /*******************************************************************
             * Cookies Not Found, No UserID and No Password found in
             * keychain/AccountAuthenticator
             ******************************************************************/
            if ((storedUserID.length == 0) || (storedPassword.length == 0)) {
                SP.WebSSOPlugin.displayLoginForm();
            }
            else {
                /***************************************************************
                 * Cookies Not Found, UserID and Password found in
                 * keychain/AccountAuthenticator
                 **************************************************************/
                var userID = decodeURIComponent(SP.WebSSOPlugin.getUserIDStoredValue());
                var password = decodeURIComponent(SP.WebSSOPlugin.getPasswordStoredValue());
                // SP.WebSSOPlugin.checkWebSSO(userID, password, "",
                // "credentialsLogin");
                SP.WebSSOPlugin.displayLoginForm();
            }
        }
        else {
            /*******************************************************************
             * Cookies Found in keychain/AccountAuthenticator
             ******************************************************************/
            // SP.WebSSOPlugin.checkWebSSO(userID, password,
            // SP.WebSSOPlugin.getCookieStoredValue(), "cookieLogin");
            
            var networkState = navigator.network.connection.type;
            var states = {};
            states[Connection.UNKNOWN] = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI] = 'WiFi connection';
            states[Connection.CELL_2G] = 'Cell 2G connection';
            states[Connection.CELL_3G] = 'Cell 3G connection';
            states[Connection.CELL_4G] = 'Cell 4G connection';
            states[Connection.NONE] = 'No network connection';
            
            var checkConnection = states[networkState];
            if ((checkConnection == 'Unknown connection') || (checkConnection == 'No network connection')) {
                SP.WebSSOPlugin.displayLoginForm();
            }
            else {
                // //////// STUDENT ONLY AUTHENTICATION
                
                if (AUTHENTICATION_STUDENTONLY == true) {
                
                    if (storedUserID.charAt(0) == 'p') {
                    
                        SP.WebSSOPlugin.checkWebSSO(storedUserID, password, SP.WebSSOPlugin.getCookieStoredValue(), "cookieLogin");
                        
                    }
                    else {
                        PARAMS_OFFLINE_USERID = "";
                        SP.WebSSOPlugin.displayLoginForm();
                    }
                    
                    // //////// STAFF ONLY AUTHENTICATION
                }
                else 
                    if (AUTHENTICATION_STAFFONLY == true) {
                    
                        if (storedUserID.charAt(0) == 's') {
                        
                            SP.WebSSOPlugin.checkWebSSO(storedUserID, password, SP.WebSSOPlugin.getCookieStoredValue(), "cookieLogin");
                            
                        }
                        else {
                            PARAMS_OFFLINE_USERID = "";
                            SP.WebSSOPlugin.displayLoginForm();
                        }
                        
                    // //////// STUDENT/STAFF AUTHENTICATION
                    }
                    else 
                        if (AUTHENTICATION_STUDENT_AND_STAFF == true) {
                            SP.WebSSOPlugin.checkWebSSO(storedUserID, password, SP.WebSSOPlugin.getCookieStoredValue(), "cookieLogin");
                            
                        // //////// NORMAL AUTHENTICATION
                        }
                        else {
                            SP.WebSSOPlugin.checkWebSSO(storedUserID, password, SP.WebSSOPlugin.getCookieStoredValue(), "cookieLogin");
                            
                        }
            }
        }
    }, /* end of checkStoredValues */
    /***************************************************************************
     * Triggers the getUserData Plugin method (Android)
     **************************************************************************/
    getUserData: function(types, success, fail){
        return cordova.exec(success, fail, "WebSSOPlugin", "getUserData", types);
    },
    /***************************************************************************
     * Triggers the saveUserData Plugin method (Android)
     **************************************************************************/
    saveUserData: function(types, success, fail){
        return cordova.exec(success, fail, "WebSSOPlugin", "saveUserData", types);
    },
    /***************************************************************************
     * Process the Encryption Keys
     **************************************************************************/
    processEncryptionKey: function(callback){
        if (device.platform == 'Android') {
            SP.WebSSOPlugin.getUserData(["key"], function(result){
                encryptionKey = result;
                if (encryptionKey == "") {
                    try {
                        /***************************************
                         * Generate Encryption Key
                         **************************************/
                        var text = "";
                        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                        for (var i = 0; i < 5; i++) {
                            text += possible.charAt(Math.floor(Math.random() *
                            possible.length));
                        }
                        encryptionKey = text;
                        /***************************************
                         * Saves Encryption Key to
                         * Keychain.AccountAtuthenticator
                         **************************************/
                        console.log("LOG: SAVE ENCRYPTION KEY: " +
                        encryptionKey);
                        SP.WebSSOPlugin.saveKey(encryptionKey);
                    } 
                    catch (err) {
                        encryptionKey = "";
                        console.log("LOG: Error Encyption Key");
                    }
                }
				callback(encryptionKey);
                console.log("LOG: WITH ENCRYPTION KEY: " +
                encryptionKey);
                
            }, function(error){
            
                try {
                    /*******************************************
                     * Generate Encryption Key
                     ******************************************/
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    for (var i = 0; i < 5; i++) {
                        text += possible.charAt(Math.floor(Math.random() *
                        possible.length));
                    }
                    encryptionKey = text;
                    SP.WebSSOPlugin.saveKey(encryptionKey);
                } 
                catch (err) {
                    encryptionKey = "";
                    console.log("LOG: Error Encyption Key");
                }
                callback(encryptionKey);
                console.log("LOG: NO ENCRYPTION KEY: " +
                encryptionKey);
            });
        }
    },
    /***************************************************************************
     * Get the user id from keychain/AccountAuthenticator
     **************************************************************************/
    getUserID: function(){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            getKeychain("userid", "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.getUserData(["userid"], function(result){
                    result = Aes.Ctr.decrypt(decodeURIComponent(result), encryptionKey, 256);
                    SP.WebSSOPlugin.setUserIDKeychainValue(result);
                    return result;
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Get the password from keychain/AccountAuthenticator
     **************************************************************************/
    getPassword: function(){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            getKeychain("password", "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.getUserData(["password"], function(result){
                    result = Aes.Ctr.decrypt(decodeURIComponent(result), encryptionKey, 256);
                    SP.WebSSOPlugin.setPasswordKeychainValue(result);
                    return result;
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Get the session cookie from keychain/AccountAuthenticator
     **************************************************************************/
    getCookie: function(){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            getKeychain("cookie", "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.getUserData(["cookie"], function(result){
                    SP.WebSSOPlugin.setCookieKeychainValue(result);
                    return result;
                }, function(error){
                });
            }
        
    },
    /***************************************************************************
     * Saves the userid to keychain/AccountAuthenticator
     **************************************************************************/
    saveUserID: function(userid){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad') || (device.platform == 'iOS')) {
            setKeychain("userid", userid, "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.setUserIDKeychainValue(userid);
                userid = Aes.Ctr.encrypt(userid, encryptionKey, 256);
                SP.WebSSOPlugin.saveUserData(["userid", userid], function(result){
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Saves the password to keychain/AccountAuthenticator
     **************************************************************************/
    savePassword: function(password){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            setKeychain("password", password, "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.setPasswordKeychainValue(password);
                password = Aes.Ctr.encrypt(password, encryptionKey, 256);
                SP.WebSSOPlugin.saveUserData(["password", password], function(result){
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Saves the session cookie to keychain/AccountAuthenticator
     **************************************************************************/
    saveCookie: function(cookie){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            setKeychain("cookie", cookie, "spkeychain");
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.setCookieKeychainValue(cookie);
                SP.WebSSOPlugin.saveUserData(["cookie", cookie], function(result){
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Saves the encryption key
     **************************************************************************/
    saveKey: function(key){
        SP.WebSSOPlugin.saveUserData(["key", key], function(result){
        }, function(error){
        });
    },
    /***************************************************************************
     * Clears all data stored in keychain/account value
     **************************************************************************/
    clearUserData: function(){
        if ((device.platform == 'iPhone Simulator') ||
        (device.platform == 'iPhone') ||
        (device.platform == 'iPad Simulator') ||
        (device.platform == 'iPad')|| (device.platform == 'iOS')) {
            removeAllStoredValues();
        }
        else 
            if (device.platform == 'Android') {
                SP.WebSSOPlugin.saveUserData(["userid", ""], function(result){
                }, function(error){
                });
                SP.WebSSOPlugin.saveUserData(["password", ""], function(result){
                }, function(error){
                });
                SP.WebSSOPlugin.saveUserData(["cookie", ""], function(result){
                }, function(error){
                });
            }
    },
    /***************************************************************************
     * Return the userID variable from keychain/account value
     **************************************************************************/
    getUserIDStoredValue: function(){
        return userIDkeychainValue;
    },
    /***************************************************************************
     * Set the userID variable from keychain/account stored value
     **************************************************************************/
    setUserIDKeychainValue: function(value){
        userIDkeychainValue = value;
    },
    /***************************************************************************
     * Return the Password variable from keychain/account stored value
     **************************************************************************/
    getPasswordStoredValue: function(){
        return passwordkeychainValue;
    },
    /***************************************************************************
     * Set the Password variable from keychain/account stored value
     **************************************************************************/
    setPasswordKeychainValue: function(value){
        passwordkeychainValue = value;
    },
    /***************************************************************************
     * Set the Cookie variable from keychain/account stored value
     **************************************************************************/
    getCookieStoredValue: function(){
        return cookiekeychainValue;
    },
    /***************************************************************************
     * Set the Cookie variable from keychain/account stored value
     **************************************************************************/
    setCookieKeychainValue: function(value){
        cookiekeychainValue = value;
    }
};
